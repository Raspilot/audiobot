# -*- coding: utf-8 -*-

"""

This file is the script to run to get the functionality of the bot.
This file contains the main function which defines what events trigger what reactions.

The bot is triggered and responds with one of the questions defined in "askfor".
The answer is then processed in "edit" which is followed by another question.

"""


import logging

from telegram.ext import CallbackQueryHandler, ConversationHandler, CommandHandler, MessageHandler
from telegram.ext import Filters, Updater

from AudioBot import init_file, init_web, init_reply, done, abort
from AudioBot import listen, update_cutting_presets, utils, start, info, setup

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)


def main():
    if not setup():
        print(f"Please modify {utils.get_dir('config_dir')} to set up the bot.")
    updater = Updater(token=utils.get_token(), use_context=True)
    updater.dispatcher.add_handler(CommandHandler("start", callback=start))
    updater.dispatcher.add_handler(CommandHandler("info", callback=info))
    updater.dispatcher.add_handler(CommandHandler("update_presets", callback=update_cutting_presets))
    updater.dispatcher.add_handler(
        ConversationHandler(
            entry_points=[
                MessageHandler(Filters.audio, init_file),
                MessageHandler(Filters.regex("https://.\S"), callback=init_web),
                MessageHandler(Filters.reply, callback=init_reply),
            ],
            states={
                "wait_attribute_selection": [CallbackQueryHandler(listen.choice),
                                             MessageHandler(Filters.reply, callback=listen.choice)],
                "wait_value": [MessageHandler(Filters.reply, callback=listen.value),
                               CallbackQueryHandler(listen.value)],
                "wait_download": [CallbackQueryHandler(listen.download)],
                "wait_cut": [CallbackQueryHandler(listen.cut_preset),
                             MessageHandler(Filters.reply, listen.cut_special)]
            },
            fallbacks=[
                MessageHandler(Filters.regex("^Done$"), done),
                MessageHandler(Filters.regex("^Abort$"), abort),
            ],
            per_message=False,
        )
    )
    updater.dispatcher.add_error_handler(utils.error_handler)
    updater.start_polling()
    print("bot started")
    updater.idle()
    print("bot stopped")
    return


if __name__ == '__main__':
    main()
