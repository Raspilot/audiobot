# AudioBot
This telegram bot waits for audio files to be sent into a group, asks for instructions on how to edit them and sends them back to the chat.

## Requirements
Before trying to install an instance of the bot yourself, make sure you have the following things installed on your computer
* [Python 3](https://www.python.org/downloads/)
* PIP
* virtualenv `python3 -m pip install virtualenv`
## Setup
### Get the bot
#### via git
```commandline
cd path/to/projects/
git clone https://gitlab.com/Raspilot/audiobot.git
```

#### via download
Go to [the project page](https://gitlab.com/Raspilot/audiobot) and download the master branch in any form you want. [Here](https://gitlab.com/Raspilot/audiobot/-/archive/master/audiobot-master.zip) is the direct link to the ZIP-file in case you cannot find the download link.
Extract the files to `path/to/projects/audiobot/`.

### Start the bot
Once the virtualenv is configured as described in [First Startup](#first-startup), use
```commandline
cd path/to/projects/audiobot/
source ./venv/bin/activate
```
Run the bot
```commandline
python3 main.py
```
If you want to run the latest version with the most up to date youtube-dl, simply copy the following line and paste it into your terminal
This command only works if you have `git` installed.
```commandline
git pull && python3 -m pip install -r requirements.txt && pip install -U youtube-dl && python3 main.py 
```


#### First Startup
On The first startup the bot asks you via cli to enter the token of your bot.
Check out the [telegram documentation](https://core.telegram.org/bots#3-how-do-i-create-a-bot) on how to create a bot and how to get a token.

You also need to set your virtual environment.

##### Step 1 - Get virtualenv

Make sure you have virtualenv installed.
```commandline
virtualenv --version
```
if it is not installed try any of the following commands
```commandline
pip install virtualenv
```
```commandline
sudo apt-get install python-virtualenv
```
```commandline
sudo easy_install virtualenv
```

##### Step 2 - Setup your virtualenv

Make sure you have python 3.8 installed. Look up the instructions for you operating system on how to do that.

```commandline
cd path/to/projects/audiobot/
which python3.8
virtualenv -p path/to/python3.8
source venv/bin/activate
python -m pip install --upgrade pip
pip install -r requirements.txt
```

##### Run the bot
```commandline
python3 main.py
```
##### Stop the bot
Press `Ctrl + C` and wait for a some seconds.

### Permissions

You can restrict access to your bot. 
This is useful if you do not want to limit your bandwidth or make the power consumption go over the top.

To edit permissions you need to edit the config.json which - unless you modified the code - is located in `~/.databass/config.json` after the first startup.

#### Public Vs. Private

Changing `is_public` will change whether you bot is public if set to `true` or private if set to `false`.

##### Public

If `is_public` is set to `true`, everyone can access your bot.

##### Private

If `is_public` is set to `false`, you need to change some more values.
* `allowed_access` needs to contain chat_ids or usernames of all the users you want to be able to use the bot.

### Get updates

Update the bot via

```commandline
cd path/to/projects/audiobot/
git pull
```

### Advanced config
The config is a json file in `~/.databass/config.json` which you can modify manually. The following attributes are useful for the bot.
* token: str -> Your bots token.
* admins: List[str] -> A list of usernames which users can turn to and eg. recommend cutting presets.
* is_public: bool -> Decide whether you bot is public or only available for chat_ids and usernames listed in "allowed_access" and "admins".
* allowed_access: List -> A list of integers which correspond to the chat_id of a chat or user names. The second option will also allow users to use the bot in groups where others are not allowed to use it.
* wants_polls: List[int] -> A list of integers which corresponds to the chat_ids of the chats which want to receive polls on a audio file
* cutting_presets: dict[List] -> A dictionary of lists with two numbers each. The first number defines how much will be cut from the beginning and the second number defines how much is removed from the end if that preset is selected.
* preview_tags: dict[str] -> A dictionary which defines the values which are written to the preview file.

See the [example config file](./example_config.json) for some examples.

#### How do i know my chat's id?
In the chat you want to use your AudioBot type "/start@<YourBotName_bot>". The chat_id to add to config.json->allowed_access will be displayed on the command line interface.


## DataBass
The private instance of the owner of this bot is called [DataBass](https://t.me/audiomanager_bot).

## Known Issues
* The bot can only handle one request at a time.
* /info can not correctly display github style markdown