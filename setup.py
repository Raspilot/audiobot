from setuptools import setup, find_packages

setup(
    name="AudioBot",
    version="1.1.1",
    description="A telegram bot for editing mp3 tags.",
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    author="Fabian Becker",
    author_email="fab.becker@outlook.de",
    url="https://gitlab.com/Raspilot/audiobot",
    download_url="https://gitlab.com/Raspilot/audiobot.git",
    packages=find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
    ],
    license="GPLv3",
    keywords="telegram python python-telegram-bot id3 mp3 tag",
    platforms="telegram python",
    python_requires='>=3.8.0',
)