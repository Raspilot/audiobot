# -*- coding: utf-8 -*-

"""

This file contains functions for interacting with third party data.

"""


import os

import requests
from bs4 import BeautifulSoup as bs


def get_video_title(url):
    if "invidio" in url:
        r = requests.get(url)
        doc = bs(r.text, "html.parser")
        return doc.find_all("h1")[0].text.strip()
    else:
        return None


def download(url, path):
    dir = '/'.join(path.split('/')[:-1])
    file = path.split('/')[-1].split('.')[0]
    os.chdir(dir)
    # youtube-dl -q --extract-audio --output $FILENAME --audio-format mp3 $LINK
    cmd = " ".join([
        "youtube-dl",
        "-q",
        "--extract-audio",
        f"--output \"{file}.%(ext)s\"",
        "--audio-format mp3",
        "--audio-quality 0",
        url
    ])
    os.system(cmd)
    file_exists = path.split("/")[-1] in os.listdir('/'.join(path.split('/')[:-1]))
    if not file_exists:
        raise FileNotFoundError("File could not be downloaded")
    return f"{path}.mp3"
