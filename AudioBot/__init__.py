# -*- coding: utf-8 -*-

"""

This file contains some functions for starting and ending conversations.

"""


import json
import os
import requests
import sys
import time
import traceback
from typing import Optional

import telegram.error
from mutagen.easyid3 import EasyID3
from telegram import ParseMode
from telegram.ext import ConversationHandler
from telegram.ext.callbackcontext import CallbackContext
from telegram.update import Update

from AudioBot import askfor
from AudioBot import utils
from AudioBot import webscrape


def setup(token: Optional[str] = None,
          config_path: Optional[str] = utils.get_dir("config_dir")):
    """An assistant to set up your bot instance."""
    # Verify config files are existent
    config_exists = os.path.exists(config_path) and json.loads(open(config_path).read() or {})
    if not config_exists:
        os.system(f"cp example_config.json {config_path}")
        if token:
            config_file = open(f"{config_path}", "rw")
            config = json.loads(config_file.read())
            config["token"] = token
            config_file.write(json.dumps(config))
            config_file.close()
    token = (
        token
        or (utils.get_token() if "token" in utils.get_config() and config_exists else None)
        or input("Please enter your bot's token: ")
    )
    utils.set_config(token=token)
    return config_exists


def start(update: Update, context: CallbackContext):
    chat_id = update.message.chat_id
    try:
        @access_control
        def check_access(**kwargs):
            return True
        has_access = check_access(**{"update": update, "context": context})
    except PermissionError:
        print(f"Unknown user {update.message.from_user} has requested access via chat {update.message.chat_id}.")
        has_access = False
    msg = context.bot.send_message(
        chat_id=chat_id,
        text=f"Hi, {update.message.from_user.first_name}."
             + f" {'I already' if has_access else 'Now I'} know this chat.\n"
             + ("You have permission to use the bot." if has_access else
                "Ask the maintainer of this bot instance to add"
                + f" {update.message.chat_id} or {update.message.from_user.username}"
                + " to the list of allowed chats."),
        reply_to_message_id=update.message.message_id,
        disable_notification=True,
    )
    if has_access:
        time.sleep(5)
        for msg_id in [msg.message_id, update.message.message_id, ]:
            context.bot.delete_message(
                chat_id=chat_id,
                message_id=msg_id,
            )


def info(update: Update, context: CallbackContext):
    doc = "https://gitlab.com/Raspilot/audiobot/-/blob/master/README.md"
    try:
        context.bot.send_message(
            text=f"Please take a look at the [online documentation]({doc})." + "\n" * 2 + open("README.md").read(),
            chat_id=update.message.chat_id,
            parse_mode=ParseMode.MARKDOWN,
        )
    except telegram.error.BadRequest:
        context.bot.send_message(
            text=f"Please look up the <a href=\"{doc}\">online documentation</a>",
            chat_id=update.message.chat_id,
            parse_mode=ParseMode.HTML
        )


def admin_only(func):
    """Only allow admins to access functions wrapped by this function"""
    def auth_and_call(*args, **kwargs):
        if "update" in kwargs and type(kwargs["update"]) == Update:
            update = kwargs["update"]
        elif len(args) >= 2 and type(args[0]) == Update:
            update = args[0]
        else:
            return
        user_name = update.message.from_user.username
        config = utils.get_config()
        has_access = any([user_name in config["admins"]])
        if has_access:
            return func(*args, **kwargs)
        else:
            print("\n".join(
                [
                    f"{update.message.from_user} tried to access admin only function {func}.",
                    f"Add {update.message.from_user.username} to {utils.get_dir()['config_dir']} -> admins"
                    + " if you believe that's an error."
                ]
            ))
            raise PermissionError("You do not have permission to use this function.")
    return auth_and_call


@admin_only
def update_cutting_presets(update: Update, context: CallbackContext):
    """This function allows for integrating"""
    url = "https://gitlab.com/Raspilot/audiobot/-/raw/master/example_config.json?inline=false"
    new_presets = json.loads(requests.get(url).text)["cutting_presets"]
    old_presets = utils.get_config()["cutting_presets"]
    config = utils.get_config()
    config["cutting_presets"] = {**new_presets, **old_presets}
    utils.set_config(**config)
    msg = context.bot.send_message(
        chat_id=update.message.chat_id,
        text="New presets have been added.",
        disable_notification=True,
    )
    time.sleep(2)
    for msg_id in [msg.message_id, update.message.message_id]:
        context.bot.delete_message(
            chat_id=update.message.chat_id,
            message_id=msg_id,
        )


def access_control(func):
    """Control access to the bot"""
    def auth_and_call(*args, **kwargs):
        if "update" in kwargs and type(kwargs["update"]) == Update:
            update = kwargs["update"]
        elif len(args) >= 2 and type(args[0]) == Update:
            update = args[0]
        else:
            return
        chat_id = update.message.chat_id
        user_name = update.message.from_user.username
        config = utils.get_config()
        has_access = any([config["is_public"],
                          chat_id in config["allowed_access"],
                          user_name in config["allowed_access"],
                          user_name in config["admins"]])
        if has_access:
            return func(*args, **kwargs)
        else:
            print("\n".join(
                [
                    f"Restrict access for chat {update.message.chat_id} (user: {update.message.from_user}).",
                    f"Add {update.message.chat_id} to {utils.get_dir()['config_dir']} -> allowed_access"
                    + " if you believe that's an error."
                ]
            ))
            raise PermissionError("You do not have permission to use this bot.")
    return auth_and_call


def init(update: Update, context: CallbackContext):
    """A init function necessary for every conversation"""
    # send a fist response message
    message = context.bot.send_message(
        text="Loading dialog",
        chat_id=update.message.chat_id,
        reply_to_message_id=update.message.message_id,
        disable_notification=True,
    )
    context.user_data["chat_id"] = update.message.chat_id
    # the bot always updates the same message to prevent spamming. This is the messages id
    context.user_data["msg_id"] = message.message_id
    # save the id of the initial message sent by the user so it can be deleted afterwards as well
    context.user_data["orig_msg"] = update.message


@access_control
def init_file(update: Update, context: CallbackContext):
    """Start a conversation based on a file"""
    context.user_data["init_type"] = "file"
    init(update, context)
    file_data = update.message.audio
    if file_data.mime_type != "audio/mpeg":
        return ConversationHandler.END
    file = context.bot.getFile(file_data.file_id)
    file_name = file_data.title
    context.user_data["file"] = file_name
    path = utils.get_dir("work_dir") + file_name
    file.download(path)
    audio_msg = context.bot.send_audio(
        chat_id=update.message.chat_id,
        audio=open(path, "rb"),
    )
    context.user_data["audio_id"] = audio_msg.message_id
    return askfor.attribute_select(context, purpose="edit")


@access_control
def init_reply(update: Update, context: CallbackContext):
    """Initializes a conversation based on a reply to a audio message"""
    context.user_data["init_type"] = "reply"
    if "edit" in update.message.text.lower() and update.message.reply_to_message.audio:
        message = context.bot.send_message(
            text="Loading dialog",
            chat_id=update.message.chat_id,
            reply_to_message_id=update.message.reply_to_message.message_id,
            disable_notification=True,
        )
        context.user_data["chat_id"] = update.message.chat_id
        # the bot always updates the same message to prevent spamming. This is the messages id
        context.user_data["msg_id"] = message.message_id
        context.user_data["orig_msg"] = update.message.reply_to_message
        context.bot.delete_message(
            chat_id=update.message.chat_id,
            message_id=update.message.message_id,
        )

        context.bot.editMessageText(
            chat_id=context.user_data["chat_id"],
            message_id=context.user_data["msg_id"],
            reply_to_message_id=update.message.reply_to_message.message_id,
            text="Reedit a audio file."
        )
        file_data = update.message.reply_to_message.audio
        if file_data.mime_type != "audio/mpeg":
            return ConversationHandler.END
        file = context.bot.getFile(file_data.file_id)
        file_name = file_data.title + ".mp3"
        context.user_data["file"] = file_name
        path = utils.get_dir("work_dir") + file_name
        file.download(path)
        audio_msg = context.bot.send_audio(
            chat_id=update.message.chat_id,
            audio=open(path, "rb"),
        )
        context.user_data["audio_id"] = audio_msg.message_id
        return askfor.attribute_select(context)


@access_control
def init_web(update: Update, context: CallbackContext):
    """Start a conversation based on a link"""
    context.user_data["init_type"] = "web"
    if " " in update.message.text:
        return ConversationHandler.END
    init(update, context)
    url = update.message.text
    context.user_data["url"] = url
    default_title = "No title detected"
    title = None
    try:
        title = utils.ease_string(webscrape.get_video_title(url) or url.split("?")[1].split("=")[1]) or default_title
    except IndexError:
        pass
    context.bot.editMessageText(
        text=f"Detected a link: {title or default_title} ({url})",
        chat_id=context.user_data["chat_id"],
        message_id=context.user_data["msg_id"],
        disable_web_page_preview=True,
    )
    context.user_data["file"] = (title or default_title) + ".mp3"
    return askfor.download(context)


def done(update: Update, context: CallbackContext):
    """End a conversation after successful editing"""
    # Do some finishing touches
    context.bot.editMessageText(
        chat_id=context.user_data["chat_id"],
        message_id=context.user_data["msg_id"],
        text="Finished editing. Your file will be sent to you soon."
    )
    # rename file
    current_filename = context.user_data["file"]
    preview_name = "preview_" + context.user_data["file"]
    context.user_data["file"] = current_filename
    path_to_file = utils.get_dir("work_dir") + current_filename
    id3 = EasyID3(path_to_file)
    if "artist" in id3.keys() and "title" in id3.keys():
        new_filename = f"{' & '.join(id3['artist'])} - {' vs. '.join(id3['title'])}.mp3"
    else:
        return askfor.attribute_select(context, note="You need to set at least artist and title!")
    if preview_name in os.listdir(utils.get_dir("work_dir")):
        utils.af_set_attributes(
            utils.get_dir("work_dir") + preview_name,
            **utils.af_get_attributes(utils.get_dir("work_dir") + current_filename)
        )
        os.renames(
            utils.get_dir("work_dir") + preview_name,
            utils.get_dir("work_dir") + current_filename
        )
    os.renames(
        utils.get_dir("work_dir") + current_filename,
        utils.get_dir("work_dir") + new_filename,
    )
    path_to_file = utils.get_dir("work_dir") + new_filename
    # send file
    audio_msg = context.bot.send_audio(
        chat_id=context.user_data["chat_id"],
        audio=open(path_to_file, "rb"),
        caption="\n".join([
            f"{x.title()} = {id3[x][0]}" for x in sorted(id3.keys())
        ])
    )
    # remove old file
    if "audio_id" in context.user_data:
        context.bot.delete_message(
            chat_id=context.user_data["chat_id"],
            message_id=context.user_data["audio_id"],
        )
    if utils.get_polls_allowed(update, context):
        # send poll
        context.bot.send_poll(
            chat_id=context.user_data["chat_id"],
            question=f"What do you think about {audio_msg.audio.title} by {audio_msg.audio.performer}?",
            options=[
                "It's nice",
                "It' better than ok but does not blow my mind",
                "It's ok",
                "Absolutely no way"
            ],
            is_anonymous=True,
            disable_notification=True,
            reply_to_message_id=audio_msg.message_id,
        )
    # delete file
    os.remove(path_to_file)
    # delete redundant messages
    context.bot.delete_message(
        chat_id=context.user_data["chat_id"],
        message_id=context.user_data["msg_id"],
    )
    # delete initial user message
    context.bot.delete_message(
        chat_id=context.user_data["chat_id"],
        message_id=context.user_data["orig_msg"].message_id,
    )
    if update.message:
        # Delete the message if user finished with "Done"
        context.bot.delete_message(
            chat_id=update.message.chat_id,
            message_id=update.message.message_id,
        )
    # delete user data
    context.user_data.clear()

    return ConversationHandler.END


def abort(update: Update, context: CallbackContext, reason: Optional[str] = None):
    """Makes the bot shut up immediately, sends a final message if there is a reason or error message."""
    try:
        print(f"Failed on init type \"{context.user_data['init_type']}\"")
        if context.error:
            traceback.print_exception(*sys.exc_info())
            if not reason:
                reason = str(context.error)
        if reason:
            # updates the message text
            if context.user_data and "msg_id" in context.user_data and "chat_id" in context.user_data:
                context.bot.editMessageText(
                    text=reason,
                    chat_id=context.user_data["chat_id"],
                    message_id=context.user_data["msg_id"],
                    reply_to_message_id=None,
                )
            else:
                context.bot.send_message(
                    text=reason,
                    chat_id=utils.get_chat(update, context),
                    reply_to_message_id=None,
                )
        else:
            # deletes the message
            context.bot.delete_message(
                chat_id=utils.get_chat(update, context),
                message_id=context.user_data["msg_id"],
            )
            if "audio_id" in context.user_data:
                context.bot.delete_message(
                    chat_id=utils.get_chat(update, context),
                    message_id=context.user_data["audio_id"]
                )
        # delete user message if the user sent "Abort"
        if update.message:
            context.bot.delete_message(
                chat_id=update.message.chat_id,
                message_id=update.message.message_id,
            )
        if "file" in context.user_data and context.user_data["file"] in os.listdir(utils.get_dir("work_dir")):
            # delete the file
            os.remove(utils.get_dir("work_dir") + context.user_data["file"])
        if "file" in context.user_data and "preview_" + context.user_data["file"] in os.listdir(utils.get_dir("work_dir")):
            # delete the file
            os.remove(utils.get_dir("work_dir") + "preview_" + context.user_data["file"])
        # delete all user data
        context.user_data.clear()
    finally:
        # end the conversation
        return ConversationHandler.END
