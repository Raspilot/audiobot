# -*- coding: utf-8 -*-

"""

This file contains the questions to ask the user.
These functions are mostly called by functions in the edit module or the __init__ module.

"""


from mutagen.easyid3 import EasyID3
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
import json

from AudioBot import CallbackContext, utils, Optional, webscrape


def attribute_choose(context: CallbackContext, next_state: str = "wait_attribute_selection"):
    context.bot.editMessageText(
        chat_id=context.user_data["chat_id"],
        message_id=context.user_data["msg_id"],
        text="What attribute do you want to edit then?",
        reply_markup=InlineKeyboardMarkup([[InlineKeyboardButton("<Back>", callback_data="_back")],
                                           [InlineKeyboardButton("<List all valid>", callback_data="_valid")]])
    )
    return next_state


def attribute_select(context: CallbackContext,
                     next_state: str = "wait_attribute_selection",
                     note: Optional[str] = None,
                     purpose: Optional[str] = "edit"):
    attr = {**{"artist": None,
               "title": None},
            **utils.af_get_attributes(utils.get_dir("work_dir") + context.user_data["file"])}
    context.bot.editMessageText(
        chat_id=context.user_data["chat_id"],
        message_id=context.user_data["msg_id"],
        text=(note + "\n" if note else "") + "Click on an attribute" + (" to " + purpose if purpose else ""),
        reply_markup=InlineKeyboardMarkup(
            utils.build_menu(
                buttons=[InlineKeyboardButton(f"{x}={attr[x]}", callback_data=x) for x in attr],
                n_cols=2,
            ) + [
                [
                    InlineKeyboardButton("<Choose another>", callback_data="_another"),
                    InlineKeyboardButton("<Cut>", callback_data="_cut"),
                ],
                [
                    InlineKeyboardButton("<Abort>", callback_data="_abort"),
                    InlineKeyboardButton("<Finish>", callback_data="_done"),
                ],
            ]
        )
    )
    return next_state


def attribute_select_all(context: CallbackContext,
                         next_state: str = "wait_attribute_selection",
                         note: Optional[str] = None,
                         purpose: Optional[str] = "edit"):
    """Returns a full list of all available attributes back to the user."""
    attr = {**{"artist": None,
               "title": None},
            **utils.af_get_attributes(utils.get_dir("work_dir") + context.user_data["file"])}
    context.bot.editMessageText(
        chat_id=context.user_data["chat_id"],
        message_id=context.user_data["msg_id"],
        text=(note + "\n" if note else "") + "Select attribute" + (" to " + purpose if purpose else ""),
        reply_markup=InlineKeyboardMarkup(
            [[InlineKeyboardButton("<Back>", callback_data="_back")]]
            + utils.build_menu(
                buttons=[
                    InlineKeyboardButton(
                        x + ('=' + attr[x] if x in attr and attr[x] is not None else ''),
                        callback_data=x)
                    for x in sorted(list(EasyID3.valid_keys.keys()))
                ],
                n_cols=2
            )
        )
    )
    return next_state


def download(context: CallbackContext):
    url = context.user_data["url"]
    try:
        v_title = webscrape.get_video_title(url)
        v_title = utils.ease_string(v_title if v_title else url.split("/")[-1].split("?")[1].split("=")[1])
        context.user_data["v_title"] = v_title
        if " - " in v_title:
            context.user_data["artist"] = v_title.split(" - ")[0]
            context.user_data["title"] = v_title.split(" - ")[1]
    except IndexError:
        print(ValueError("Something went wrong reading the video title."))
        v_title = None
        context.user_data["v_title"] = v_title
    context.bot.editMessageText(
        text=f"Detected: {v_title or '<title could not be detected automatically>'}",
        chat_id=context.user_data["chat_id"],
        message_id=context.user_data["msg_id"],
        reply_markup=InlineKeyboardMarkup(
            [
                [InlineKeyboardButton("Download", callback_data="download"),
                 InlineKeyboardButton("Ignore", callback_data="ignore")]
            ]
        ),
    )
    return "wait_download"


def value(context):
    attr = utils.af_get_attributes(utils.get_dir("work_dir") + context.user_data["file"])
    choice = context.user_data["choice"]
    context.bot.editMessageText(
        chat_id=context.user_data["chat_id"],
        message_id=context.user_data["msg_id"],
        text="\n".join([
            f"Ok, now send me the value you want '{choice}' to be."
            + " Make sure to send it via an answer and not a regular message",
            f"{choice} = {attr[choice]}"
        ]),
        reply_markup=InlineKeyboardMarkup(
            [
                [
                    InlineKeyboardButton("<Back>", callback_data="_back"),
                    InlineKeyboardButton("<Delete>", callback_data="_delete"),
                ],
            ]
        )
    )
    return "wait_value"


def cutting_pattern(context):
    presets = utils.get_config()["cutting_presets"]
    admins = ["@" + a for a in utils.get_config()["admins"]]
    text = "\n".join(
        [
            "Please select one of the following presets.",
            "If none of them fit your needs,"
            + " type the time in milliseconds you want to cut from"
            + " the beginning and the end separated by a whitespace, and recommend it to"
            + (" " + ", ".join(admins)) or " the admin of your bot instance"
            + " or even better the developer."
        ]
    )
    context.bot.editMessageText(
        chat_id=context.user_data["chat_id"],
        message_id=context.user_data["msg_id"],
        text=text,
        reply_markup=InlineKeyboardMarkup(
            utils.build_menu(
                [
                    InlineKeyboardButton(p, callback_data=json.dumps(presets[p])) for p in presets
                ],
                n_cols=2
            )
        )
    )
    return "wait_cut"
