# -*- coding: utf-8 -*-

"""

This file contains the reactions on events.
Most functions end in a question from the askfor module.

"""

from AudioBot import *


def download(update: Update, context: CallbackContext):
    if update.callback_query.data == "download":
        context.bot.editMessageText(
            text="Downloading file. Lean back as this might take a while.",
            chat_id=context.user_data["chat_id"],
            message_id=context.user_data["msg_id"],
        )
        try:
            path = utils.get_dir("work_dir") + context.user_data["file"]
            try:
                webscrape.download(
                    context.user_data["url"],
                    path
                )
            except FileNotFoundError as e:
                utils.error_log(e)
                # abort
                return abort(
                    update=update,
                    context=context,
                    reason="There has been an error downloading the file."
                )
            context.bot.editMessageText(
                text="Download was successful. You will receive a preview shortly.",
                chat_id=context.user_data["chat_id"],
                message_id=context.user_data["msg_id"],
            )
            if context.user_data["v_title"]:
                utils.af_set_attributes(
                    utils.get_dir("work_dir") + context.user_data["file"],
                    **utils.get_config()["preview_tags"],
                    )
                audio_msg = context.bot.send_audio(
                    chat_id=context.user_data["chat_id"],
                    audio=open(path, "rb"),
                )
                context.user_data["audio_id"] = audio_msg.message_id
                utils.af_set_attributes(
                    utils.get_dir("work_dir") + context.user_data["file"],
                    title=context.user_data["title"] if "title" in context.user_data else context.user_data["v_title"],
                    **{"artist": context.user_data["artist"]} if "artist" in context.user_data else {}
                )
            return askfor.attribute_select(context)
        except OSError:
            return abort(update, context, "Downloading files is not implemented yet.")
    elif update.callback_query.data == "ignore":
        return abort(update, context)


def choice(update: Update, context: CallbackContext):
    if update.message:
        context.user_data["choice"] = update.message.text
        context.bot.delete_message(
            chat_id=context.user_data["chat_id"],
            message_id=update.message.message_id,
        )
    else:
        context.user_data["choice"] = update.callback_query.data
        if update.callback_query.data == "_another":
            return askfor.attribute_choose(context)
        if update.callback_query.data == "_back":
            return askfor.attribute_select(context)
        if update.callback_query.data == "_valid":
            return askfor.attribute_select_all(context)
        if update.callback_query.data == "_done":
            return done(update, context)
        if update.callback_query.data == "_abort":
            return abort(update, context)
        if update.callback_query.data == "_cut":
            return askfor.cutting_pattern(context)
    return askfor.value(context)


def value(update: Update, context: CallbackContext):
    if update.message and update.message.reply_to_message.message_id == context.user_data["msg_id"]:
        if update.message:
            context.user_data["value"] = update.message.text
            context.bot.editMessageText(
                chat_id=context.user_data["chat_id"],
                message_id=context.user_data["msg_id"],
                text="Editing file..."
            )
            f = EasyID3(utils.get_dir("work_dir") + context.user_data["file"])
            f[context.user_data["choice"].lower()] = context.user_data["value"]
            f.save()
            context.bot.delete_message(
                chat_id=context.user_data["chat_id"],
                message_id=update.message.message_id,
            )
    else:
        if update.callback_query.data == "_delete":
            return del_attribute(context)
        if update.callback_query.data == "_back":
            return askfor.attribute_select(context)
    return askfor.attribute_select(context)


def del_attribute(context: CallbackContext):
    f = EasyID3(utils.get_dir("work_dir") + context.user_data["file"])
    f[context.user_data["choice"]] = ""
    f.save()
    try:
        assert context.user_data["choice"] not in EasyID3(utils.get_dir("work_dir") + context.user_data["file"]).keys()
        return askfor.attribute_select(context, note=f"Deleted attribute \"{context.user_data['choice']}\"")
    except AssertionError:
        return askfor.attribute_select(context, note=f"Failed to delete \"{context.user_data['choice']}\"")


def cut_special(update: Update, context: CallbackContext):
    context.bot.editMessageText(
        chat_id=context.user_data["chat_id"],
        message_id=context.user_data["msg_id"],
        text="Your custom cutting pattern is going to be applied to the file."
    )
    context.bot.delete_message(
        chat_id=update.message.chat_id,
        message_id=update.message.message_id
    )
    return cut(
        context,
        begin=float(update.message.text.split()[0]),
        end=float(update.message.text.split()[1]),
    )


def cut_preset(update: Update, context: CallbackContext):
    context.bot.editMessageText(
        chat_id=context.user_data["chat_id"],
        message_id=context.user_data["msg_id"],
        text="Your selected cutting pattern is going to be applied to the file."
    )
    return cut(
        context,
        begin=json.loads(update.callback_query.data)[0],
        end=json.loads(update.callback_query.data)[1],
    )


def cut(context: CallbackContext, begin, end):
    path = utils.get_dir("work_dir") + "preview_" + context.user_data["file"]
    utils.cut_audio(
        path=utils.get_dir("work_dir") + context.user_data["file"],
        begin=begin,
        end=end,
        store_as=path
    )
    context.bot.delete_message(
        chat_id=context.user_data["chat_id"],
        message_id=context.user_data["audio_id"],
    )
    audio_msg = context.bot.send_audio(
        chat_id=context.user_data["chat_id"],
        audio=open(path, "rb"),
        disable_notification=True,
    )
    context.user_data["audio_id"] = audio_msg.message_id
    return askfor.attribute_select(context)
