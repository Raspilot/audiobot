# -*- coding: utf-8 -*-

"""

This file contains reactions on updates by the user.

"""


import json
import os
import string
import sys
import traceback

import telegram.error
from mutagen.easyid3 import EasyID3
from pydub import AudioSegment
import AudioBot


def af_get_attributes(path: str):
    """Returns the attributes of an mp3 file as dict."""
    af = EasyID3(path)
    tags_all = {
            **{"album": None,
               "artist": None,
               "title": None,
               "genre": None},
            **{x.lower(): " & ".join(af[x]) for x in af.keys()}}
    tags = {
        x: tags_all[x] for x in tags_all
        if x not in ["ENCODING"]
    }
    return tags


def af_set_attributes(path: str, **val: dict):
    """Returns the attributes of an mp3 file as dict."""
    af = EasyID3(path)
    for v in val:
        try:
            if val[v]:
                af[v] = val[v]
        except KeyError as e:
            error_handler(error=e, abort=False)
    af.save()


def cut_audio(path, begin, end, store_as=None):
    audio = AudioSegment.from_mp3(path)[begin:-max(end, 1)]
    audio.export(store_as or path, format=path.split(".")[-1], tags=get_config()["preview_tags"])


def build_menu(buttons,
               n_cols,
               header_buttons=None,
               footer_buttons=None):
    menu = [buttons[i:i + n_cols] for i in range(0, len(buttons), n_cols)]
    if header_buttons:
        menu.insert(0, [header_buttons])
    if footer_buttons:
        menu.append([footer_buttons])
    return menu


def ease_string(text: str):
    ok = "".join([string.ascii_letters, string.digits, "()[].-+&?=\\\"'", " "])
    text = "".join([a for a in text if a in ok])
    while "  " in text:
        text = text.replace("  ", " ")
    return text


def error_handler(update=None, context=None, abort=True, error=None):
    print("New error")
    error = context.error if context else error or Exception
    try:
        raise error
    except telegram.error.NetworkError:
        print("telegram.error.NetworkError")
        # do not abort
        abort = False
    finally:
        error_log(error)
        if abort:
            return AudioBot.abort(update, context)


def error_log(error):
    error_log_path = get_dir("home") + "error_log.txt"
    # noinspection PyBroadException
    try:
        raise error
    except Exception:
        error_info = sys.exc_info()
        with open(error_log_path, "a") as f:
            f.write("new_error\n")
            traceback.print_exception(file=f, *error_info)
        print(f"New error has been logged to error log {error_log_path}")


def get_dir(folder="home"):
    base = os.path.expanduser("~/.databass/")
    work_dir = base + "music/"
    config_dir = base + "config.json"
    for p in [work_dir, config_dir]:
        if not os.path.exists("/".join(p.split("/")[:-1])):
            os.makedirs(p)
    res = {
        "home": base,
        "work_dir": work_dir,
        "config_dir": config_dir
    }
    if folder:
        return res[folder]
    else:
        return res


def get_config(config_path=get_dir("config_dir")):
    config = json.loads(open(config_path).read() or "{}")
    return config


def set_config(config_path=None, **config):
    combined = get_config()
    for x in config:
        combined[x] = config[x]
    with open(config_path or get_dir("config_dir"), "w") as f:
        f.write(json.dumps(combined, indent=4))
        f.close()


def get_token(config_path=get_dir("config_dir")):
    return get_config(config_path)["token"]


def get_chat(update, context):
    if update.message and update.message.chat_id:
        return update.message.chat_id
    elif "chat_id" in context.user_data:
        return context.user_data["chat_id"]
    return None


def get_polls_allowed(update, context):
    return get_chat(update, context) in get_config()["wants_polls"]


if __name__ == '__main__':
    from AudioBot import webscrape
    print(ease_string(webscrape.get_video_title("https://invidio.us/watch?v=y0BytZz8Nns")))
